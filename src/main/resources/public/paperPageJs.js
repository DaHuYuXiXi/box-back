var obj_temp;
var obj_index;
//paper相关的js代码实现
layui.use('table', function(){
    var table = layui.table;
      var $=layui.jquery;
    //展示所有的纸条数据
    table.render({
        elem: '#paperData'
        ,height: 900
        ,url: 'get_all_papers'
        ,page: true //开启分页
        ,method:'post'
        ,cols: [
            [ //表头
            {field: 'p_id', title: '纸条ID', width:100, sort: true, fixed: 'left'}
            ,{field: 'p_u_id', title: '纸条对应用户ID', width:130}
                ,{field: 'uname', title: '纸条对应的用户姓名', width: 170}
                ,{field: 'sex', title: '纸条对应的用户性别',templet: '#sexShow',width: 170}
                ,{field: 'upDateTime', title: '纸条的上传时间',templet: '#upTimeShow',width: 250}
            ,{field: 'contactData', title: '联系方式', width:165, sort: true}
            ,{field: 'selfInfo', title: '自我介绍', width:250}
                ,{field: 'selfImgs', title: '上传的自拍照',templet: '#paperImgsShow', width:200},
                {fixed: 'right', width:178, title: "删除纸条",align:'center', toolbar: '#barDemo'}
        ]
        ]
        , limits: [3, 5, 10]  //一页选择显示3,5或10条数据
        , limit: 5  //一页显示10条数据
        , parseData: function (res) { //将原始数据解析成 table 组件所规定的数据，res为从url中get到的数据
            var result;
            if (this.page.curr) {
                result = res.data.slice(this.limit * (this.page.curr - 1), this.limit * this.page.curr);
            } else {
                result = res.data.slice(0, this.limit);
            }
            return {
                "code": 0, //解析接口状态
                "msg": "ok", //解析提示文本
                "count": res.data.length, //解析数据长度
                "data": result //解析数据列表
            };
        }
    });


    //工具条事件
    table.on('tool(paperFilter)', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
        obj_temp=obj;

       if(layEvent === 'del'){ //删除
           var index1=layer.confirm('<font color=red>删除该纸条</font>', {
               btn: ['确认', '取消']
               ,yes: function(index, layero)
               {
                   obj_index=index;
                   //发送ajax
                   delPaperById(obj.data.p_id)
               },
               btn2: function (index,layero)
               {

               }
           });
           layer.title("<font color=red>确认删除</font>",index1)
        }
    });
});

//删除纸条的方法
function delPaperById(p_id)
{
    $.ajax({
        url: "del_paper_by_id",
        type: 'post',
        data: {"p_id":p_id},
        success: function (res)
        {
            if(res.status==="handle_success")
            {
                obj_temp.del(); //删除对应行（tr）的DOM结构，并更新缓存
                layer.close(obj_index);
                layer.msg('删除成功', {icon: 1});
                 return;
            }
            layer.msg('删除失败', {icon: 5});
        }
    })
}


