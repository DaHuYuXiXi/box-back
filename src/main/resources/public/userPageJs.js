//user相关的js代码实现
layui.use(['table','form'], function(){
    var table = layui.table;
    var $=layui.jquery;
    var form = layui.form;
    //展示所有的纸条数据
    table.render({
        elem: '#userData'
        ,height: 600
        ,url: 'get_all_users'
        ,method:'post'
        ,cols: [
            [ //表头
                {field: 'id', title: '用户ID', width:100, sort: true, fixed: 'left'}
                ,{field: 'uname', title: '姓名', width: 170}
                ,{field: 'upwd', title: '密码',width: 150},
                ,{field: 'sex', title: '性别',templet: '#sexShow',width: 90},
                ,{field: 'mail', title: '邮箱', width:165, sort: true}
                ,{field: 'lauchNum', title: '当日已投放纸条数量', width:170}
                ,{field: 'takeNum', title: '当日已拿取纸条数量', width:170}
                ,{field: 'flag', title: '<font color=red>封号</font>',templet: '#bannedFlag', width:150}
                ,{fixed: 'right', title: '<font color=red>删除</font>',width:178, align:'center', toolbar: '#removeUserById'}
            ]
        ]
        ,page: true //开启分页
        , limits: [3, 5, 10]  //一页选择显示3,5或10条数据
        , limit: 5  //一页显示10条数据
        , parseData: function (res) { //将原始数据解析成 table 组件所规定的数据，res为从url中get到的数据
            var result;
            if (this.page.curr) {
                result = res.data.slice(this.limit * (this.page.curr - 1), this.limit * this.page.curr);
            } else {
                result = res.data.slice(0, this.limit);
            }
            return {
                "code": 0, //解析接口状态
                "msg": "ok", //解析提示文本
                "count": res.data.length, //解析数据长度
                "data": result //解析数据列表
            };
        }
    });


    //修改开关的状态
    //1:封号
    //0:不封号
    function checkStatus(flag,data)
    {
        if(flag==1)
        {
            // 设置为原始状态（恢复状态）
            data.elem.checked =true;
            // 渲染方法1：渲染全部表单的checkbox
            form.render('checkbox');
        }
        else if(flag==0)
        {
            // 设置为原始状态（恢复状态）
            data.elem.checked =false;
            // 渲染方法1：渲染全部表单的checkbox
            form.render('checkbox');
        }
    }

    //监听封号操作
    form.on('checkbox(lockDemo)', function(obj){

        console.log(obj.elem.checked)
        console.log(obj.othis)
        var chose=obj.elem.checked;//判断当前状态
        if(!chose)//解封
        {
            //解封
            var index=layer.confirm('<font color=#adff2f>确认解封？</font>', {
                btn: ['确认', '取消']
                ,yes: function(index, layero){
                    //发送请求
                    $.ajax({
                        url:'remove_banned_user_by_id',
                        type:'post',
                        data:{"u_id":obj.elem.attributes.u_id.nodeValue},
                        success:function (res)
                        {
                            if(res.status==="handle_success")
                            {
                                obj.value=0
                                layer.msg('成功', {icon: 1});
                                return;
                            }
                            if(res.status==="database_error")
                            {
                                checkStatus(1,obj);
                                layer.msg('数据库操作出现异常', {icon: 5});
                                return;
                            }
                            checkStatus(0,obj);
                        }
                        ,error:function ()
                        {
                            checkStatus(1,obj);
                            layer.msg('数据库操作出现异常', {icon: 5});
                        }
                    })
                },
                //取消解封，还是封号
                btn2: function (index,layero)
                {
                    checkStatus(1,obj);
                },
                //如果直接关闭弹出层，那么还是需要回复到下架状态
                cancel: function(index, layero){
                    //下架
                    checkStatus(1,obj);
                }
            });
            layer.title("<font color=#adff2f>解封</font>",index)
        }
        else//封号
        {
            var index=layer.confirm('<font color=red>确认封号？</font>', {
                btn: ['确认', '取消']
                ,yes: function(index, layero){
                    //发送请求
                    $.ajax({
                        url:'banned_user_by_id',
                        type:'post',
                        data:{"u_id":obj.elem.attributes.u_id.nodeValue},
                        success:function (res)
                        {
                            if(res.status==="handle_success")
                            {
                                obj.value=1;
                                layer.msg('成功', {icon: 1});
                                return;
                            }
                            checkStatus(0,obj);
                            layer.msg('数据库或者文件操作出现异常', {icon: 1});
                        }
                        ,error:function ()
                        {
                            checkStatus(0,obj);
                            layer.msg('数据库或者文件操作出现异常', {icon: 1});
                        }
                    })
                },
                //取消封号，还是不封号状态
                btn2: function (index,layero)
                {
                    checkStatus(0,obj);
                    obj.value=0;
                },
                //如果直接关闭弹出层，那么还是需要回复到下架状态
                cancel: function(index, layero){
                    //下架
                    checkStatus(0,obj);
                }
            });

            layer.title("<font color=red>封号</font>",index)

        }
    });

    //工具条事件
    table.on('tool(userFilter)', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
        obj_temp=obj;
        if(layEvent === 'del'){ //删除
            var index1=layer.confirm('<font color=red>删除用户</font>', {
                btn: ['确认', '取消']
                ,yes: function(index, layero)
                {
                    obj_index=index;
                    //发送ajax
                    delUserById(obj.data.id)
                },
                btn2: function (index,layero)
                {

                }
            });
            layer.title("<font color=red>确认删除</font>",index1)
        }
    });

});

//删除纸条的方法
function delUserById(u_id)
{
    $.ajax({
        url: "del_user_by_id",
        type: 'post',
        data: {"u_id":u_id},
        success: function (res)
        {
            if(res.status==="handle_success")
            {
                obj_temp.del(); //删除对应行（tr）的DOM结构，并更新缓存
                layer.close(obj_index);
                layer.msg('删除成功', {icon: 1});
                return;
            }
            layer.msg('删除失败', {icon: 5});
        }
    })
}
