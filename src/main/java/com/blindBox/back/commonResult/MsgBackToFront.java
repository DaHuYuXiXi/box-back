package com.blindBox.back.commonResult;

import lombok.Data;

//返回给前端的通用消息类
@Data
public class MsgBackToFront
{
    //返回的数据
    private Object data;
    //返回的状态
    private String status;
    //返回给前端的状态
    public final static String LOGIN_ERROR="login_error";//用户名或者密码错误
    public final static String TOKEN_ERROR="token_error";//token错误
    public final static String EXCEPTION_ERROR="exception_error";//出现异常
    public final static String HANDLE_SUCCESS ="handle_success";//处理成功
    public final static String REGISTER_REPEAT="register_repeat";//用户名重复
    public final static String DATABASE_ERROR="database_error";//数据库操作出现异常
    public final static String NONE_PAPER="none_paper";//没有纸条可以抽取
    public final static String NO_SELF_IMGS="no_self_imgs";//没有上传自拍照
    public final static String DEL_FILE_ERROR="del_file_error";//文件删除失败


    public MsgBackToFront(Object data, String status)
    {
        this.data=data;
        this.status=status;
    }

}
