package com.blindBox.back.dataStrategy;

import com.blindBox.back.commonResult.MsgBackToFront;
import com.blindBox.back.dataStrategy.method.UserDataMethod;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

//获取用户数据的策略
@Component("userGetDataStrategy")
public class UserGetDataStrategy extends AbstractUserDataStrategy
{
    public UserGetDataStrategy(UserDataMethod userDataMethod) {
        super(userDataMethod);
    }

    @Override
    public MsgBackToFront getUserDataByChoseStrategy(Object[] objs)
    {
        Method method = strategyMappedMethod.get(this.strategy);
            try {
                    //结果
                    //首先获取每个参数的类型
                Class<?>[] types = method.getParameterTypes();
                Object ret = method.invoke(userDataMethod, objs);
                    //处理成功
                return new MsgBackToFront(ret,MsgBackToFront.HANDLE_SUCCESS);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            //出现异常
            return new MsgBackToFront(null,MsgBackToFront.EXCEPTION_ERROR);
    }

    @Override
    public MsgBackToFront getUserDataByChoseStrategy() {
        Method method = strategyMappedMethod.get(this.strategy);
        try {
            //结果
            Object ret = method.invoke(this.userDataMethod);
            //处理成功
            return new MsgBackToFront(ret,MsgBackToFront.HANDLE_SUCCESS);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //出现异常
        return new MsgBackToFront(null,MsgBackToFront.EXCEPTION_ERROR);
    }

}
