package com.blindBox.back.dataStrategy;

import com.blindBox.back.commonResult.MsgBackToFront;
import com.blindBox.back.dataStrategy.method.UserDataMethod;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

//用户数据抽象策略层
public abstract class AbstractUserDataStrategy
{
    //策略
    public static final String GET_ALL_USERS="/get_all_users"; //获取所有通过账号注册的用户
    public static final String GET_MAN_USERS="/get_man_users";//获取男性用户
    public static final String GET_WOMAN_USERS="/get_woman_users"; //获取女性用户
    public static final String GET_RATE_RET="/get_rate_ret"; //获取男女用户比例
    public static final String GET_USER_NUM="/get_user_num";//获取总用户数量
    //记录选择的策略
    protected volatile String strategy;
    //注入策略的实现方法
    protected UserDataMethod userDataMethod;
    //容器存储每种方法和对应策略的映射关系
    protected Map<String, Method> strategyMappedMethod=new ConcurrentHashMap<>();
   @Autowired
    public AbstractUserDataStrategy(UserDataMethod userDataMethod)
    {
        this.userDataMethod=userDataMethod;
        //策略和指定方法的映射
        Method[] methods = userDataMethod.getClass().getMethods();
        for (Method method : methods)
        {
           strategyMappedMethod.put("/"+method.getName(),method);
        }
    }
    //指定某种策略
    public AbstractUserDataStrategy chose(String strategy)
    {
        this.strategy=strategy;
        return this;
    }
    //使用指定的策略，返回相应的结果，方法有参数
    public abstract MsgBackToFront getUserDataByChoseStrategy(Object[] objs);
    //无参
    public abstract MsgBackToFront getUserDataByChoseStrategy();
}
