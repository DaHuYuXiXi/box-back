package com.blindBox.back.dataStrategy;

import com.blindBox.back.commonResult.MsgBackToFront;
import com.blindBox.back.dataStrategy.method.PaperDataMethod;
import com.blindBox.back.dataStrategy.method.UserDataMethod;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

//获取纸条数据的策略
@Component("paperGetDataStrategy")
public class PaperGetDataStrategy extends AbstractPaperDataStrategy
{
    public PaperGetDataStrategy(PaperDataMethod paperDataMethod) {
        super(paperDataMethod);
    }

    @Override
    public MsgBackToFront getPaperDataByChoseStrategy(Object[] objs) {
        Method method = strategyMappedMethod.get(this.strategy);
        try {
            //结果
            Object ret = method.invoke(paperDataMethod, objs);
            //处理成功
            return new MsgBackToFront(ret,MsgBackToFront.HANDLE_SUCCESS);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //出现异常
        return new MsgBackToFront(null,MsgBackToFront.EXCEPTION_ERROR);
    }

    @Override
    public MsgBackToFront getPaperDataByChoseStrategy() {
        Method method = strategyMappedMethod.get(this.strategy);
        try {
            //结果
            Object ret = method.invoke(this.paperDataMethod);
            //处理成功
            return new MsgBackToFront(ret,MsgBackToFront.HANDLE_SUCCESS);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //出现异常
        return new MsgBackToFront(null,MsgBackToFront.EXCEPTION_ERROR);
    }
}
