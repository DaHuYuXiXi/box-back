package com.blindBox.back.dataStrategy.method;

import com.blindBox.back.parse.PaperParse;
import com.blindBox.back.service.PaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
@Component
public class PaperDataMethod
{
    @Autowired
   private PaperService paperService;
    @Autowired
    private PaperParse paperParse;
    //总纸条数量
    public Integer get_paper_num()
    {
        return paperService.getPaperNums();
    }
    //可用纸条数量
    public Integer get_paper_able_num()
    {
        return paperService.getAblePaperNums();
    }
    //获取所有纸条完整信息
    public List<Map<Object,Object>> get_all_papers()
    {
        List<Map<Object, Object>> papersInfo = paperService.getPapersInfo();
        //纸条内容解密--为了照顾没有被加密过的纸条
        try
        {
            List<Map<Object, Object>> afterParse=new ArrayList<>();
            for (Map<Object, Object> paper : papersInfo)
            {
                String selfInfo = (String)paper.get("selfInfo");
                String parse = paperParse.parse(selfInfo);
                paper.put("selfInfo",parse);
                afterParse.add(paper);
            }
            return afterParse;
        }
        catch (Exception e)
        {
            System.out.println("???????????");
            System.out.println("纸条解密失败");
            System.out.println("???????????");
        }
        //返回当前用户抽取到的所有纸条
        return papersInfo;
    }
}
