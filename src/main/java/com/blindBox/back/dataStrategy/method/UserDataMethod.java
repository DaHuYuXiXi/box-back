package com.blindBox.back.dataStrategy.method;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blindBox.back.dao_pojo.Paper;
import com.blindBox.back.dao_pojo.User;
import com.blindBox.back.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserDataMethod
{
    @Autowired
    private UserService userService;
    //获取所有的用户
    public List<User> get_all_users()
    {
        return userService.list();
    }
    //获取男性用户
    public List<User> get_man_users()
    {
        //1：男
        return userService.list(new QueryWrapper<User>().eq("sex",1));
    }
    //获取女性用户
    public List<User> get_woman_users()
    {
        //0：女
        return userService.list(new QueryWrapper<User>().eq("sex",0));
    }
    //获取男女用户比例
      public double get_rate_ret()
      {
        //查询男性用户的数量
          double manNums = userService.getManNums();
          //查询女性用户的数量
          double womanNums = userService.getWomanNums();
          return (manNums/(manNums+womanNums))*100;
      }

    //获取总用户数量
    public Integer get_user_num()
    {
        return  userService.getAllUsersNum();
    }

    //通过纸条id获取到拥有者的信息
    public User get_paper_user_info(Integer u_Id){return userService.getById(u_Id);}

    //获取抽中纸条者的信息
    public User get_chose_paper_user_info(String u_name){return userService.getOne(new QueryWrapper<User>().eq("uname",u_name));}
}
