package com.blindBox.back.dataStrategy;

import com.blindBox.back.commonResult.MsgBackToFront;
import com.blindBox.back.dataStrategy.method.PaperDataMethod;
import com.blindBox.back.dataStrategy.method.UserDataMethod;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//纸条数据抽象策略层
public abstract class AbstractPaperDataStrategy
{
    //策略
    public static final String GET_PAPER_NUM="get_paper_num";//总纸条数量
    public static final String GET_PAPER_ABLE_NUM="get_paper_able_num";//剩余可用纸条数量
    public static final String GET_ALL_PAPERS="get_all_papers";//获取所有纸条详细信息
    //记录选择的策略
    protected volatile String strategy;
    //注入策略的实现方法
    protected PaperDataMethod paperDataMethod;
    //容器存储每种方法和对应策略的映射关系
    protected Map<String, Method> strategyMappedMethod=new ConcurrentHashMap<>();
    @Autowired
    public AbstractPaperDataStrategy(PaperDataMethod paperDataMethod)
    {
        this.paperDataMethod=paperDataMethod;
        //策略和指定方法的映射
        Method[] methods = paperDataMethod.getClass().getMethods();
        for (Method method : methods)
        {
            strategyMappedMethod.put("/"+method.getName(),method);
        }
    }
    //指定某种策略
    public AbstractPaperDataStrategy chose(String strategy)
    {
        this.strategy=strategy;
        return this;
    }
    //使用指定的策略，返回相应的结果，方法有参数
    public abstract MsgBackToFront getPaperDataByChoseStrategy(Object[] objs);
    //无参
    public abstract MsgBackToFront getPaperDataByChoseStrategy();
}
