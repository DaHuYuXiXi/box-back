package com.blindBox.back.dataHandler;

//数据处理
public interface AbstractDataHandler<T,R>
{
    //处理数据的方法
    R dataHandle(T obj);
    R dataHandle(T obj,Object[] objs);
}
