package com.blindBox.back.dataHandler.dataHandlerImpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blindBox.back.commonResult.MsgBackToFront;
import com.blindBox.back.dao_pojo.Paper;
import com.blindBox.back.dao_pojo.Paperchosebywho;
import com.blindBox.back.dao_pojo.User;
import com.blindBox.back.dataHandler.AbstractDataHandler;
import com.blindBox.back.dataStrategy.AbstractUserDataStrategy;
import com.blindBox.back.file.FileManager;
import com.blindBox.back.service.PaperService;
import com.blindBox.back.service.PaperchosebywhoService;
import com.blindBox.back.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Component("userDataHandler")
public class UserDataHandler implements AbstractDataHandler<String,MsgBackToFront>
{
    @Resource(name="userGetDataStrategy")
    private AbstractUserDataStrategy userGetDataStrategy;
   @Autowired
   private UserService userService;
   @Autowired
   private PaperService paperService;
   @Autowired
   private FileManager fileManager;
   @Autowired
   private PaperchosebywhoService paperchosebywhoService;

    @Override
    public MsgBackToFront dataHandle(String strategy)
    {
        //选择策略，获取结果
        return userGetDataStrategy.chose(strategy).getUserDataByChoseStrategy();
    }

    @Override
    public MsgBackToFront dataHandle(String strategy, Object[] objs)
    {
        //选择策略，获取结果
        return userGetDataStrategy.chose(strategy).getUserDataByChoseStrategy(objs);
    }

    //封禁用户
    public MsgBackToFront banUserById(Integer u_id)
    {
        //封禁用户，并删除当前用户发布的所有纸条
        User user=new User();
        user.setFlag(1);
        user.setId(u_id);
        userService.updateById(user);
        //删除用户发布的纸条
        //1.查出当前用户对应的所有纸条信息
        Map<String, Object> map = new HashMap<>();
        map.put("userId",user.getId());
        List<Paper> papers = paperService.listByMap(map);
        //2.先删除每张纸条对应上传的图片
        //3.删除每张纸条的数据库记录
        for (Paper paper : papers) {
            fileManager.delTargetFile(paper.getSelfImgs());
            paperchosebywhoService.remove(new QueryWrapper<Paperchosebywho>().eq("p_id",paper.getId()));
            paperService.removeById(paper.getId());
        }
        return new MsgBackToFront(null,MsgBackToFront.HANDLE_SUCCESS);
    }

    //解封
    public MsgBackToFront removceBanUserById(Integer u_id) {
        //数据库记录解封
        User user=new User();
        user.setFlag(0);
        user.setId(u_id);
        return userService.updateById(user)?new MsgBackToFront(null,MsgBackToFront.HANDLE_SUCCESS):new MsgBackToFront(null,MsgBackToFront.DATABASE_ERROR);
    }

    //删除用户
    public MsgBackToFront delUserById(Integer u_id) {
        banUserById(u_id);
        //删除该用户
        userService.removeById(u_id);
       return new MsgBackToFront(null,MsgBackToFront.HANDLE_SUCCESS);
    }
}
