package com.blindBox.back.config.webConfig;

import com.blindBox.back.parse.PathParse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer
{
    @Autowired
    private PathParse pathParse;
    //静态资源路径映射
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        registry.addResourceHandler("/img/**").addResourceLocations("file:"+pathParse.get_paper_selfImgs_file_path());
    }
}
