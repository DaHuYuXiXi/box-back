package com.blindBox.back.parse;


import com.blindBox.back.utils.TokenTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//纸条内容加密和解密
@Component
public class PaperParse
{
    @Autowired
    private TokenTool tokenTool;
    //自我介绍加密
    public String encipher(String content)
    {
        return tokenTool.encipher(content, 100000);
    }
    //自我介绍的解密
    public String parse(String str)
    {
        return tokenTool.parse(str);
    }
}
