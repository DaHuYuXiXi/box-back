package com.blindBox.back.parse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;

//路径解析类
@Component
public class PathParse
{
    //自拍照文件存放路径
    @Value("${paper.selfImgs.windows}")
    private String selfImgs_window;
    @Value("${paper.selfImgs.linux}")
    private String selfImgs_linux;
    //判断当前环境的类
    @Autowired
    private EnvParse envParse;

    //获取自拍照文件的路径
    public String do_get_paper_selfImgs_file_path()
    {
        return envParse.isWindowsOrLinux()?selfImgs_window:selfImgs_linux;
    }

    public String get_paper_selfImgs_file_path()
    {
        String filePath = do_get_paper_selfImgs_file_path();
        System.out.println("========================");
        System.out.println(filePath);
        System.out.println("===========================");
        File file=new File(filePath);
        if(!file.exists())
        {
            //创建多级目录
            file.mkdirs();
        }
        return filePath;
    }
}

