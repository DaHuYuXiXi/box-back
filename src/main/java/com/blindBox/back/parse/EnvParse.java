package com.blindBox.back.parse;

import org.springframework.stereotype.Component;


//判断是linux环境还是window环境
@Component
public class EnvParse
{
    //判断操作系统类型
    //window:true
    //linux:false
    public Boolean isWindowsOrLinux()
    {
        boolean windows = System.getProperty("os.name").contains("Windows");
        return windows;
    }
}
