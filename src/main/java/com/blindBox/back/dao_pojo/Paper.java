package com.blindBox.back.dao_pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Paper implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 联系方式
     */
    @TableField("contactData")
    private String contactdata;

    /**
     * 自我介绍
     */
    @TableField("selfInfo")
    private String selfinfo;

    /**
     * 当前纸条的投掷者
     */
    @TableField("userId")
    private Integer userid;

    /*
    * 当前纸条是否被逻辑删除
    * */
    @TableField("del")
    private Integer del;

    //默认值为none，表示当前用户没有上传自拍照
    @TableField("selfImgs")
    private String selfImgs;

    //用户纸条上传的时间
    @TableField("upDateTime")
    private LocalDateTime upDateTime;
}
