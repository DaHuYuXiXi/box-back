package com.blindBox.back.dao_pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    @TableField("uname")
    private String uname;
    @TableField("upwd")
    private String upwd;
    @TableField("virtualImgs")
    private String virtualImgs;
    /*
    * 1:男
    * 0:女
    * */
    @TableField("sex")
    private Integer sex;
    @TableField("mail")
    private String mail;
    //用户每日抽取盲盒的次数,可以为空，默认值为0
    @TableField("lauchNum")
    private  Integer lauchNum;
    //用户每日投递盲盒的次数，可以为空，默认值为0
    @TableField("takeNum")
    private Integer takeNum;
    //是否封号---默认为0，不封号，1：封号
    @TableField("flag")
    private Integer flag;
}
