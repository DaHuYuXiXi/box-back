package com.blindBox.back.dao_pojo;

import lombok.Data;

//纸条和拿到纸条人的信息映射
@Data
public class PaperMappedToChoseUser
{
    //纸条id
    private String p_id;
    //拿到纸条的人的id
    private String chose_user_id;
}
