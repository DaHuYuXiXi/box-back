package com.blindBox.back.dao_pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Paperchosebywho
{
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //纸条id
    @TableField("p_id")
    private Integer p_id;

    //抽中纸条的用户id,默认为-1，表示当前纸条没有被别抽到
    @TableField("chose_user_id")
    private Integer chose_user_id;

    //纸条被抽到的时间
    @TableField("chose_date")
    private LocalDateTime chose_date;
}
