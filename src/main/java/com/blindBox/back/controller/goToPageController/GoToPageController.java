package com.blindBox.back.controller.goToPageController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GoToPageController
{
    //跳转到主页
     @RequestMapping("/")
    public String go_to_index()
    {
        return "login";
    }

    //跳转到主页
    @RequestMapping("/to_main")
    public String go_to_main()
    {
        return "index";
    }

    //跳到到用户数据显示页面
    @RequestMapping("/toUserDataPage")
    public String go_to_user_data_page()
    {
        return "userData";
    }

    //跳到到纸条数据显示页面
    @RequestMapping("/toPaperDataPage")
    public String go_to_paper_data_page()
    {
        return "paperData";
    }

}
