package com.blindBox.back.controller.loginController;

import com.blindBox.back.commonResult.MsgBackToFront;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController
{
  @PostMapping("/login_back")
  public MsgBackToFront verify_login(@RequestParam("name")String name, @RequestParam("pwd")String pwd)
  {
        if(name.equals("123")&&pwd.equals("123"))
        {
            return new MsgBackToFront(null,MsgBackToFront.HANDLE_SUCCESS);
        }
        return new MsgBackToFront(null,MsgBackToFront.LOGIN_ERROR);
  }
}
