package com.blindBox.back.controller.getDataController;
import com.blindBox.back.commonResult.MsgBackToFront;
import com.blindBox.back.dao_pojo.User;
import com.blindBox.back.dataHandler.AbstractDataHandler;
import com.blindBox.back.dataHandler.dataHandlerImpl.UserDataHandler;
import com.blindBox.back.dataStrategy.method.UserDataMethod;
import com.blindBox.back.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

//获取后台需要的用户数据
@RestController
public class BackDataUserController
{
    @Resource(name="userDataHandler")
    private AbstractDataHandler userDataHandler;

    //获取所有通过账号注册的用户
    @PostMapping("/get_all_users")
    public MsgBackToFront getAllUsers(HttpServletRequest  request)
    {
        return (MsgBackToFront) userDataHandler.dataHandle(request.getRequestURI());
    }

    //获取男性用户
    @PostMapping("/get_man_users")
    public MsgBackToFront getManUsers(HttpServletRequest  request)
    {
        //1：男
        return (MsgBackToFront) userDataHandler.dataHandle(request.getRequestURI());
    }

    //获取女性用户
    @PostMapping("/get_woman_users")
    public MsgBackToFront getWomanUsers(HttpServletRequest request)
    {
        //0：女
        return (MsgBackToFront) userDataHandler.dataHandle(request.getRequestURI());
    }

    //获取男女用户比例
     @PostMapping("/get_rate_ret")
     public MsgBackToFront getRate(HttpServletRequest  request)
   {
       //返回的是男生占总人数的百分比
      return (MsgBackToFront) userDataHandler.dataHandle(request.getRequestURI());
    }

    //获取总用户数量
    @PostMapping("/get_user_num")
    public MsgBackToFront getUserNums(HttpServletRequest request)
    {
        //返回用户总数
        return (MsgBackToFront) userDataHandler.dataHandle(request.getRequestURI());
    }

    @Autowired
    private UserDataMethod userDataMethod;
    //通过纸条id获取到拥有者的信息
    @PostMapping("get_paper_user_info")
    public MsgBackToFront get_paper_user_info(@RequestParam("p_u_id")Integer p_u_id)
    {
        User paper_user_info = userDataMethod.get_paper_user_info(p_u_id);
        if(paper_user_info==null)return new MsgBackToFront(null,MsgBackToFront.DATABASE_ERROR);
        return new MsgBackToFront(paper_user_info,MsgBackToFront.HANDLE_SUCCESS);
    }

    //获取抽中纸条人的性别
    @PostMapping("get_chose_user_sex")
    public MsgBackToFront get_chose_user_sex(@RequestParam("chose_name")String chose_name)
    {
        User get_chose_user_sex = userDataMethod.get_chose_paper_user_info(chose_name);
        if(get_chose_user_sex==null)return new MsgBackToFront(null,MsgBackToFront.DATABASE_ERROR);
        return new MsgBackToFront(get_chose_user_sex,MsgBackToFront.HANDLE_SUCCESS);
    }

    //封号
    @PostMapping("/banned_user_by_id")
    public MsgBackToFront banned_user_by_id(@RequestParam("u_id")Integer u_id)
    {
        UserDataHandler userDataHandler = (UserDataHandler) this.userDataHandler;
    return userDataHandler.banUserById(u_id);
     }

     //解封
    @PostMapping("/remove_banned_user_by_id")
    public MsgBackToFront remove_banned_user_by_id(@RequestParam("u_id")Integer u_id)
    {
        UserDataHandler userDataHandler = (UserDataHandler) this.userDataHandler;
        return userDataHandler.removceBanUserById(u_id);
    }

    //删除用户
    @PostMapping("/del_user_by_id")
    public MsgBackToFront del_user_by_id(@RequestParam("u_id")Integer u_id)
    {
        UserDataHandler userDataHandler = (UserDataHandler) this.userDataHandler;
        return userDataHandler.delUserById(u_id);
    }
}
