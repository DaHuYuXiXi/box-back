package com.blindBox.back.controller.getDataController;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blindBox.back.commonResult.MsgBackToFront;
import com.blindBox.back.dao_pojo.Paper;
import com.blindBox.back.dataHandler.dataHandlerImpl.PaperDataHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

//后台获取纸条需要的数据
@RestController
public class BackDataPaperController
{
    @Resource(name="paperDataHandler")
    private PaperDataHandler paperDataHandler;

    //总纸条数量
    @PostMapping("get_paper_num")
    public MsgBackToFront getAllPapers(HttpServletRequest request)
    {
        return paperDataHandler.dataHandle(request.getRequestURI());
    }

    //剩余可用纸条数量
    @PostMapping("get_paper_able_num")
    public MsgBackToFront getAblePaperNums(HttpServletRequest request)
    {
        return paperDataHandler.dataHandle(request.getRequestURI());
    }

    //获取所有纸条
    @PostMapping("get_all_papers")
    public MsgBackToFront getAllPapersInfo(HttpServletRequest request)
    {
        return paperDataHandler.dataHandle(request.getRequestURI());
    }

    //删除纸条
    @PostMapping("del_paper_by_id")
    public MsgBackToFront delPaperById(@RequestParam("p_id")Integer p_id)
    {
        return paperDataHandler.delPaperById(p_id);
    }

}
