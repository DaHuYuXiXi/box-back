package com.blindBox.back.dataModel;

import lombok.Data;

//男女比例的模型
@Data
public class RateModel
{
    private  String man_value;//男性用户占比
    private String womane_value;//女性用户占比
}
