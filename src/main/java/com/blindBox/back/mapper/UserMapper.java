package com.blindBox.back.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blindBox.back.dao_pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    //查询男性用户的数量
     Integer getManNums();
    //查询女性用户的数量
    Integer getWomanNums();
    //查询总用户数量
    Integer  getAllUsersNum();
}
