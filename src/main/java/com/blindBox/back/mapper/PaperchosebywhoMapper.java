package com.blindBox.back.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blindBox.back.dao_pojo.Paperchosebywho;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface PaperchosebywhoMapper extends BaseMapper<Paperchosebywho>
{
}
