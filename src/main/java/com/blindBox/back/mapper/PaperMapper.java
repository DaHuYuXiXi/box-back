package com.blindBox.back.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blindBox.back.dao_pojo.Paper;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
@Mapper
public interface PaperMapper extends BaseMapper<Paper>
{
  //查询出所有未被抽取的纸条的id集合
  public List<Integer> getPaperIdsByList();
  //恢复所有纸条为未被抽取的状态
  public void emptyAllPaperStatus();
  //总纸条数量
  public Integer getPaperNums();
  //查询出剩余可用的纸条数量
  public Integer getAblePaperNums();
  //查询出所有纸条的详细信息
  @MapKey("id")
  public List<Map<Object,Object>> getPapersInfo();
}
