package com.blindBox.back.service;



import com.baomidou.mybatisplus.extension.service.IService;
import com.blindBox.back.dao_pojo.Paper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public interface PaperService extends IService<Paper> {
    List<Integer> getPaperIdsByList();
    public void emptyAllPaperStatus();
    public Integer getPaperNums();
    public Integer getAblePaperNums();
    public List<Map<Object,Object>> getPapersInfo();

}
