package com.blindBox.back.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.blindBox.back.dao_pojo.User;

public interface UserService extends IService<User> {
    //查询男性用户的数量
    public Integer getManNums();
    //查询女性用户的数量
    public Integer getWomanNums();
    //查询总用户数量
    Integer  getAllUsersNum();
}
