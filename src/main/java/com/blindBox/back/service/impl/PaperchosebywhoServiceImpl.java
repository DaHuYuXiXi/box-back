package com.blindBox.back.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blindBox.back.dao_pojo.Paperchosebywho;
import com.blindBox.back.mapper.PaperchosebywhoMapper;
import com.blindBox.back.service.PaperchosebywhoService;
import org.springframework.stereotype.Service;


@Service
public class PaperchosebywhoServiceImpl extends ServiceImpl<PaperchosebywhoMapper, Paperchosebywho> implements PaperchosebywhoService
{

}
