package com.blindBox.back.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blindBox.back.dao_pojo.Paper;
import com.blindBox.back.mapper.PaperMapper;
import com.blindBox.back.service.PaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Transactional
@Service
public class PaperServiceImpl extends ServiceImpl<PaperMapper, Paper> implements PaperService {

    @Autowired
    private PaperMapper paperMapper;

    @Override
    public List<Integer> getPaperIdsByList() {
        return paperMapper.getPaperIdsByList();
    }

    @Override
    public void emptyAllPaperStatus() {
        paperMapper.emptyAllPaperStatus();
    }

    @Override
    public Integer getPaperNums() {
        return paperMapper.getPaperNums();
    }

    @Override
    public Integer getAblePaperNums() {
        return paperMapper.getAblePaperNums();
    }

    @Override
    public List<Map<Object, Object>> getPapersInfo() {
        return paperMapper.getPapersInfo();
    }
}
