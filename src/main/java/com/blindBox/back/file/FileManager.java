package com.blindBox.back.file;

import com.blindBox.back.commonResult.MsgBackToFront;
import com.blindBox.back.parse.PathParse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

//文件管理类
@Component
public class FileManager
{
    @Autowired
    private PathParse pathParse;
    //处理上传的多张图片
    public synchronized MsgBackToFront handleSelfImgs(MultipartFile[] files)
    {
        //保存所有图片的路径
        List<String> imgsPath=new ArrayList<>();
        try {
            //当前的时间戳
            long epochMilli = Instant.now().toEpochMilli();
            //自拍照路径字符串
            String self_imgs_str="";
            //没有上传自拍照
            if(files.length==0)
                return new MsgBackToFront(null,MsgBackToFront.NO_SELF_IMGS);
            //上传了自拍照
            for (MultipartFile file : files)
            {
                //数据库保存的路径
                String cur_selfImg_name=epochMilli + file.getOriginalFilename();
                //文件路径
                String cur_selfImg_file_path=pathParse.get_paper_selfImgs_file_path()+cur_selfImg_name;
                //遍历获取每张图片的名字
                //图片加上时间戳，防止名字重复
                self_imgs_str += cur_selfImg_name;
                self_imgs_str += " ";
                //将每一张图片都放到指定的目录下面
                File img = new File(cur_selfImg_file_path);
                file.transferTo(img);
                //如果上传失败，就将之前上传的通通删除
                if(!img.exists())
                {
                    //其中有一张图片上传失败，就将之前上传的图片删除
                    for (String path : imgsPath)
                    {
                       File f=new File(path);
                       if(f.exists())
                       f.delete();
                    }
                    return new MsgBackToFront(null,MsgBackToFront.EXCEPTION_ERROR);
                }
                //上传成功,保存图片的路径
                imgsPath.add(cur_selfImg_file_path);
            }
            //处理成功,返回给前端图片的路径
            return new MsgBackToFront(self_imgs_str,MsgBackToFront.HANDLE_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //出现异常
        return new MsgBackToFront(null,MsgBackToFront.EXCEPTION_ERROR);
    }

    //处理单张照片
    public synchronized MsgBackToFront handleSelfImgs(MultipartFile file)
    {
        try {
            //当前的时间戳
            long epochMilli = Instant.now().toEpochMilli();
            //自拍照路径字符串
            //数据库保存的路径
           String self_imgs_str=epochMilli + file.getOriginalFilename();
            //文件路径
            String cur_selfImg_file_path=pathParse.get_paper_selfImgs_file_path()+self_imgs_str;
            //将图片都放到指定的目录下面
            File img = new File(cur_selfImg_file_path);
            System.out.println("========================");
            System.out.println(img.getAbsolutePath());
            file.transferTo(img);
            //处理成功,返回给前端图片的路径
            return new MsgBackToFront(self_imgs_str,MsgBackToFront.HANDLE_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //出现异常
        return new MsgBackToFront(null,MsgBackToFront.EXCEPTION_ERROR);
    }

    //递归删除一个当前目录及下面的所有文件
    public synchronized void delAllFile(File file)
    {
        if(file.isFile()||file.list().length==0)
        {
            file.delete();
        }
        else
        {
            for (File f : file.listFiles()) {
                delAllFile(f); // 递归删除每一个文件
            }
            file.delete(); // 删除文件夹
        }
    }

    //删除指定的文件
    public synchronized  boolean delTargetFile(File file)
    {
        if(!file.exists())
            return true;
        return file.delete();
    }

    public synchronized  boolean delTargetFile(String img_name)
    {
        File file=new File(pathParse.get_paper_selfImgs_file_path()+img_name);
        System.out.println(file.getAbsolutePath());
        if(!file.exists())
            return true;
        return file.delete();
    }

     //返回照片路径的前缀
    public String getImgsPrefix()
    {
        return pathParse.get_paper_selfImgs_file_path();
    }



}
