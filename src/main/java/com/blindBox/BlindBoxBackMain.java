package com.blindBox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlindBoxBackMain
{
    public static void main(String[] args) {
        SpringApplication.run(BlindBoxBackMain.class,args);
    }
}
